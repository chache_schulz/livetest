'use strict';

/**
 * @ngdoc function
 * @name projectApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the projectApp
 */
angular.module('projectApp')
  .controller('MainCtrl', function ($scope, weatherService) {
    weatherService.getWeatherData('London', 'us').then(function (response) {
      $scope.weatherData = response.data.list.filter(function (day) {
        return day.dt_txt.endsWith('12:00:00');
      });
      $scope.today = $scope.weatherData[0];
    },
      function (reason) {
        $scope.error = reason;
      });


    $scope.changeToday = function (day) {
      $scope.today = day;
    };
  });
