'use strict';

/**
 * @ngdoc service
 * @name projectApp.weather.api
 * @description
 * # weather.api
 * Service in the projectApp.
 */
angular.module('projectApp')
  .service('weatherService', function ($http) {
    var apiKey = 'b1b15e88fa797225412429c1c50c122a1';

    function getWeatherData(city, country) {
      var host = 'http://samples.openweathermap.org/data/2.5/forecast?';
      var q = city + ',' + country;
      var url = host + 'q=' + q + '&appid=' + apiKey;

      return $http.get(url);
    }

    return {
      getWeatherData: getWeatherData
    };
  });
