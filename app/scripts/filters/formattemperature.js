'use strict';

/**
 * @ngdoc filter
 * @name projectApp.filter:formatTemperature
 * @function
 * @description
 * # formatTemperature
 * Filter in the projectApp.
 */
angular.module('projectApp')
  .filter('formatTemperature', function () {
    return function (input, scale, label) {
      var degreesSymbol = '\u00B0';

      function convertKelvinToCelisus(value) {
        return Math.round(value - 273.15);
      }

      function convertCelsiusToKelvin(value) {
        return Math.round(value + 273.15);
      }

      function addDegreesSymbol(value) {
        return value += degreesSymbol;
      }

      var value = parseInt(input, 10),
        convertedValue;

      if (isNaN(value)) { return input; }

      if (scale === 'F') {
        convertedValue = convertCelsiusToKelvin(value);
      }
      else if (scale === 'C') {
        convertedValue = convertKelvinToCelisus(value);
      } else {
        throw new Error('Not a valid scale');
      }

      return label ? addDegreesSymbol(convertedValue) : convertedValue;

    };
  });
