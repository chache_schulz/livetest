'use strict';

describe('Service: weather.api', function () {

  // load the service's module
  beforeEach(module('projectApp'));

  // instantiate service
  var weatherApi;
  beforeEach(inject(function (_weatherApi_) {
    weatherApi = _weatherApi_;
  }));

  it('should do something', function () {
    expect(!!weatherApi).toBe(true);
  });

});
